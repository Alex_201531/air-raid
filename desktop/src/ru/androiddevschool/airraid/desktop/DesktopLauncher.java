package ru.androiddevschool.airraid.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import ru.androiddevschool.airraid.AirRaid;
import ru.androiddevschool.airraid.Utils.Values;

public class DesktopLauncher {
	public static void main (String[] arg) {
		System.setProperty("user.name", "EnglishWords");
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = Values.WORLD_WIDTH;
		config.height = Values.WORLD_HEIGHT;
		config.title = "Air Raid";
		new LwjglApplication(AirRaid.get(), config);
	}
}
