package ru.androiddevschool.airraid;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.HashMap;

import javax.swing.text.PlainDocument;

import ru.androiddevschool.airraid.Screens.Help;
import ru.androiddevschool.airraid.Screens.Menu;
import ru.androiddevschool.airraid.Screens.Play;
import ru.androiddevschool.airraid.Screens.Settings;
import ru.androiddevschool.airraid.Utils.Assets;
import ru.androiddevschool.airraid.Utils.GameState;
import ru.androiddevschool.airraid.Utils.ScreenName;

import static ru.androiddevschool.airraid.Utils.ScreenName.HELP;
import static ru.androiddevschool.airraid.Utils.ScreenName.MENU;
import static ru.androiddevschool.airraid.Utils.ScreenName.PLAY;
import static ru.androiddevschool.airraid.Utils.ScreenName.SETTINGS;

public class AirRaid extends Game {
    private static AirRaid instanse = new AirRaid();

    public static AirRaid get() {
        return instanse;
    }

    private AirRaid() {
    }

    ;

    @Override
    public void create() {
        Gdx.gl.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        batch = new SpriteBatch();
        Assets.get();
        screens = new HashMap<ScreenName, Screen>();
        screens.put(MENU, new Menu(batch));
        screens.put(PLAY, new Play(batch));
        screens.put(SETTINGS, new Settings(batch));
        screens.put(HELP, new Help(batch));
        setScreen(MENU);
    }

    public void setScreen(ScreenName name) {
        if (screens.containsKey(name))
            setScreen(screens.get(name));
    }

    public SpriteBatch batch;
    public HashMap<ScreenName, Screen> screens;

}
