package ru.androiddevschool.airraid.Utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import java.util.HashMap;

/**
 * Created by 01k1402 on 14.03.2017.
 */
public class Assets {
    private static Assets ourInstance = new Assets();

    public static Assets get() {
        return ourInstance;
    }

    private Assets() {
        initGraphics();
        initFonts();
        initStyles();
    }

    private void initFonts() {
        fonts = new HashMap<String, BitmapFont>();
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(file("/fonts/space age.ttf"));
        FreeTypeFontParameter parameters = new FreeTypeFontParameter();
        parameters.size = 20;
        parameters.shadowColor = Color.GRAY;
        parameters.shadowOffsetX = 1;
        parameters.shadowOffsetY = 2;
        parameters.color = Color.BLUE;
        fonts.put("blue shaded", generator.generateFont(parameters));
    }

    private void initStyles() {
        buttonStyles = new HashMap<String, Button.ButtonStyle>();
        buttonStyles.put("long green checkable", new Button.ButtonStyle(
                        images.get("/UI/green_button00"),
                        images.get("/UI/green_button01"),
                        images.get("/UI/green_button03")
                )
        );
        buttonStyles.put("long green", new Button.ButtonStyle(
                        images.get("/UI/green_button00"),
                        images.get("/UI/green_button01"),
                        null
                )
        );
        buttonStyles.put("small green checkable", new Button.ButtonStyle(
                        images.get("/UI/green_button07"),
                        images.get("/UI/green_button08"),
                        images.get("/UI/green_button10")
                )
        );
        buttonStyles.put("small green", new Button.ButtonStyle(
                        images.get("/UI/green_button07"),
                        images.get("/UI/green_button08"),
                        null
                )
        );
        buttonStyles.put("long green text",
                new TextButton.TextButtonStyle(
                        images.get("/UI/green_button00"),
                        images.get("/UI/green_button01"),
                        null,
                        fonts.get("blue shaded")
                )
        );
        buttonStyles.put("long green text checkable",
                new TextButton.TextButtonStyle(
                        images.get("/UI/green_button00"),
                        images.get("/UI/green_button01"),
                        images.get("/UI/green_button03"),
                        fonts.get("blue shaded")
                )
        );
        buttonStyles.put("exit small green",
                new ImageButton.ImageButtonStyle(
                        images.get("/UI/green_button07"),
                        images.get("/UI/green_button08"),
                        null,
                        images.get("/UI/red_cross"),
                        null,
                        null
                )
        );
        buttonStyles.put("exit small green checkable",
                new ImageButton.ImageButtonStyle(
                        images.get("/UI/green_button07"),
                        images.get("/UI/green_button08"),
                        images.get("/UI/green_button10"),
                        images.get("/UI/red_cross"),
                        null,
                        null
                )
        );
    }

    private void initGraphics() {
        images = new HashMap<String, TextureRegionDrawable>();
        loadFolder(file("/"), "");
    }

    public void loadFolder(FileHandle folder, String prefix) {
        for (FileHandle file : folder.list())
            if (file.isDirectory()) {
                loadFolder(file, prefix + "/" + file.name());
            } else {
                if (file.extension().equals("jpg") || file.extension().equals("png")) {
                    images.put(prefix + "/" + file.nameWithoutExtension(), makeDr(file));
                    //System.out.println(prefix + "/" + file.nameWithoutExtension());
                }
            }
    }

    private FileHandle file(String path) {
        return Gdx.files.local(path);
    }

    private TextureRegionDrawable makeDr(FileHandle file) {
        return new TextureRegionDrawable(new TextureRegion(new Texture(file)));
    }

    public HashMap<String, TextureRegionDrawable> images;
    public HashMap<String, Button.ButtonStyle> buttonStyles;
    public HashMap<String, BitmapFont> fonts;
}
