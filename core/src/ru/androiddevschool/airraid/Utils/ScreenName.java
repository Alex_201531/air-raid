package ru.androiddevschool.airraid.Utils;

/**
 * Created by 01k1402 on 14.03.2017.
 */
public enum ScreenName {
    PLAY,
    MENU,
    SETTINGS,
    HELP,
    PAUSE
}
