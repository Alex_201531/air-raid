package ru.androiddevschool.airraid.Controller;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import ru.androiddevschool.airraid.AirRaid;
import ru.androiddevschool.airraid.Utils.ScreenName;

/**
 * Created by 01k1402 on 14.03.2017.
 */
public class ScreenTraveler extends ClickListener {
    private ScreenName name;
    public ScreenTraveler (ScreenName name){
        this.name = name;
    }
    public void clicked(InputEvent event, float x, float y){
        AirRaid.get().setScreen(name);
    }
}
