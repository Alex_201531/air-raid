package ru.androiddevschool.airraid.Screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

import ru.androiddevschool.airraid.Controller.ScreenTraveler;
import ru.androiddevschool.airraid.Utils.Assets;
import ru.androiddevschool.airraid.Utils.ScreenName;
import ru.androiddevschool.airraid.Utils.Values;

import static ru.androiddevschool.airraid.Utils.ScreenName.MENU;

/**
 * Created by 01k1402 on 31.01.2017.
 */
public class Play extends StdScreen{
    public Play(SpriteBatch batch) {
        super(batch);
        Button button;
        Table layout = new Table();
        layout.setFillParent(true);
        button = new ImageButton((ImageButton.ImageButtonStyle) Assets.get().buttonStyles.get("exit small green"));
        button.addListener(new ScreenTraveler(MENU));
        layout.add(button).padTop(10).padRight(10).row();
        ui.addActor(layout);
    }
}
