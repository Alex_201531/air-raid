package ru.androiddevschool.airraid.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;

import ru.androiddevschool.airraid.Utils.GameState;
import ru.androiddevschool.airraid.Utils.Values;

import static ru.androiddevschool.airraid.Utils.GameState.*;

/**
 * Created by 01k1402 on 14.03.2017.
 */
class StdScreen implements Screen {
    public OrthographicCamera stageCamera;
    public OrthographicCamera uiCamera;
    public Stage stage; //сцена для актеров
    public Stage ui; //сцена для кнопок
    private InputMultiplexer multiplexer;
    public GameState screenState;
    public StdScreen(SpriteBatch batch){
        stageCamera = new OrthographicCamera();
        stageCamera.setToOrtho(false);
        stage = new Stage(new FitViewport(Values.WORLD_WIDTH, Values.WORLD_HEIGHT, stageCamera), batch);
        uiCamera = new OrthographicCamera();
        uiCamera.setToOrtho(false);
        ui = new Stage(new FitViewport(Values.WORLD_WIDTH, Values.WORLD_HEIGHT, uiCamera), batch);

    }
    @Override
    public void show() {
        multiplexer = new InputMultiplexer(); //во время показывания экрана устанавливаем обработчики ввода
        multiplexer.addProcessor(ui);
        multiplexer.addProcessor(stage);
        Gdx.input.setInputProcessor(multiplexer);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        ui.act(delta);
        if (screenState == PLAY) stage.act(delta);
        stage.draw();
        ui.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().setScreenSize(width,height);
        ui.getViewport().setScreenSize(width,height);
    }

    @Override
    public void pause() {
        screenState = PAUSE;
    }

    @Override
    public void resume() {
        screenState = PLAY;
    }

    @Override
    public void hide() {
        screenState = PAUSE;
    }

    @Override
    public void dispose() {
        screenState = EXIT;
        stage.dispose();
    }
}
