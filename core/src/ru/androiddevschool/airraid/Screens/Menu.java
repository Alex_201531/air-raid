package ru.androiddevschool.airraid.Screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

import ru.androiddevschool.airraid.Controller.ScreenTraveler;
import ru.androiddevschool.airraid.Utils.Assets;
import ru.androiddevschool.airraid.Utils.ScreenName;

import static ru.androiddevschool.airraid.Utils.ScreenName.*;

public class Menu extends StdScreen{

    public Menu(SpriteBatch batch) {
        super(batch);
        Button button;
        Table layout = new Table();
        layout.setFillParent(true);

        button = new TextButton("PLAY", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("long green text"));
        button.addListener(new ScreenTraveler(PLAY));
        layout.add(button).padBottom(20).row();

        button = new TextButton("HELP", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("long green text"));
        button.addListener(new ScreenTraveler(HELP));
        layout.add(button).padBottom(20).row();

        button = new TextButton("SETTINGS", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("long green text"));
        button.addListener(new ScreenTraveler(SETTINGS));
        layout.add(button).row();

        ui.addActor(layout);
    }
}